# Helm charts repository

## Usage

`helm repo add villytiger https://villytiger.gitlab.io/helm-charts`

## List of charts

* [Cruc](https://gitlab.com/villytiger/cruc) - Customizable webhook for Kubernetes

## TODO

* Add a job for removing old development versions
